// jquery on ready event
$(function () {

    // Модель одного работника
    var Person = Backbone.Model.extend({
        defaults: {
            name: 'Фамилия',
            job: 'Должность',
            cash: 10,
            'class': 'progress-bar-success'
        },
        validate: function (attrs, options) {
            if (attrs.cash >= 100) {
                return " победитель!!!";
            }
        }
    });


    // Список работников
    var Elhow = Backbone.Collection.extend({
        model: Person
    });

    var elhow = new Elhow([
        {
            'name': 'Ekaterina',
            'job': 'Manager',
            'class': 'progress-bar-success'
        },
        {
            name: 'Alexey Orlov',
            job: 'Developer',
            'class': 'progress-bar-warning'
        },
        {
            'name': 'Alexey Krainov',
            'job': 'QA',
            'class': 'progress-bar-info'
        }
    ]);

    // представление отображение информации об одном работнике
    var PersonView = Backbone.View.extend({
        wrap: $('.panel-body'),
        className: 'progress-bar progress-bar-striped',
        tagName: 'div',
        template: _.template('<span class="sr-only">Зарплата: <%= name %> - <%= job %> - <%= cash %> т.р.</span>'),
        attributes: function () {
            return {
                'role': 'progressbar',
                'aria-valuenow': this.model.get('cash'),
                'aria-valuemin': 0,
                'aria-valuemax': 100
            };
        },
        initialize: function () {
            this.render();
        },
        render: function () {

            this.$el.html(this.template(this.model.toJSON()));

            this.wrap.prepend(this.el);
            $(this.el).css('width', this.model.get('cash') + '%').addClass(this.model.get('class')).wrap('<div class="progress"></div>');
            return this;
        },
        change: function () {

            var a = this.template(this.model.toJSON());
            this.$el.parent().replaceWith(this.$el.html(a));
            $(this.el).css({'width': this.model.get('cash') + '%'}).addClass(this.model.get('class')).wrap('<div class="progress"></div>');
        }
    });

    // представление для списка работников
    var ListPerson = Backbone.View.extend({
        collection: elhow,
        initialize: function () {
            this.render();
        },
        render: function () {
            this.collection.each(function (person) {

                var personView = new PersonView({model: person});
                person.on('change', function (model) {
                    $('.alert').html('Зарплата у ' + model.get('name') + ' теперь ' + model.changed.cash + ' т.р.');
                    personView.change();
                });

                person.on("invalid", function (model, error) {
                    $('.btn-primary').attr('disabled', true);
                    model.set('cash', 100);
                    $('.alert').html(model.get('name') + error).removeClass('alert-info').addClass('alert-success');

                });
            })
        }
    });

    var ListPerson = new ListPerson();

    $('.btn-primary').on('click', function () {
        var button = $(this);
        button.attr('disabled', true);

        var n = Math.floor(Math.random() * elhow.length);
        var pandPerson = elhow.at(n);
        var cash = +pandPerson.get('cash');
        var sum = +Math.floor(Math.random() * (10) + 1) + cash;


        setTimeout(function () {
            button.attr('disabled', false);
            pandPerson.set('cash', sum, {validate: true});
        }, 1000);
    });
});
